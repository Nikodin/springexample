package com.example.springexample.controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class FirstController {

    //Testing out the root page as the examples.
    @GetMapping("/")
    public String index() {
        return "This is the root page";
    }

    //Greeting along with SysUserName, with a message.
    @GetMapping("/greeting")
    public String greeting() {
            return "Hello " + System.getProperty("user.name") +  ", this is as bad as it gets!";
        }

    //Takes in the Pathvariable and returns a message included.
    @RequestMapping(value = "/greeting/{name}", method = RequestMethod.GET)
    public String greetingName(@PathVariable String name) {
        return "Hello, " + name + " this is as bad as it gets!";
    }

    //Takes the pathvariable "input" as a string, and checks if it's a Palindrome.
    //Example: http://localhost:8080/palindrome?query=abba
    @RequestMapping(value = "/palindrome", method = RequestMethod.GET)
    public String checkPalindrome (@RequestParam("query") String input) {
        for (int i = 0, j = input.length() - 1; j >= i; i++, j--) {
            if (input.charAt(i) != input.charAt(j))
                return input + "Is not a palindrome" ;
        }
        return input + "Is a palindrome";
    }
}










