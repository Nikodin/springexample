# SpringExample

### How it works: 
Boots up different URL's for different purposes as to test reading, input and communication on the back end side. 

A greeting prompt when user enters /greeting at the end of the local url. Greeting followed by the username. 

A specific name greeted, same greeting as above but with a specific target name that the user can enter  
at the end of the url like "/greeting/"Lucas" - will greet Lucas. 

A Palindrome check for a entered query, enter /palindrome?query="PUT A WORD HERE" for the function to check if the  
entered word is a palindrome or not!


*See comments within the code for details*
### About: 
A simple assignment to test the basics of Boot Spring Java. 
